package main

//Knut Jørgen Totland - 494331

//Imports
import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
)

var startTime = time.Now()

//Struct for first part of species by country request getting country info
type Country struct {
	CountryCode string `json:"alpha2Code"`
	CountryName string `json:"name"`
	CountryFlag string `json:"flag"`
}

//Struct for first second of species by country request getting species info
type Species struct {
	SpeciesKey int    `json:"speciesKey"`
	Species    string `json:"species"`
}

//Struct for combining both of the parts of country request
type speciesOutput struct {
	CountryCode string   `json:"code"`
	CountryName string   `json:"countryname"`
	CountryFlag string   `json:"countryflag"`
	Species     []string `json:"species"`
	SpeciesKey  []int    `json:"speciesKey"`
}

//Struct to gather the results
type Results struct {
	Result []Species `json:"results"`
}

//Struct for first part of single species getting info in first request
type SingleSpecies struct {
	SpeciesKey     int    `json:"key"`
	SpeciesKingdom string `json:"kingdom"`
	SpeciesPhylum  string `json:"phylum"`
	SpeciesOrder   string `json:"order"`
	SpeciesFamily  string `json:"family"`
	Speciesgenus   string `json:"genus"`
	SpeciesSciName string `json:"scientificName"`
	SpeciesCanName string `json:"canonicalName"`
	SpeciesYear    string `json:"year"`
}

//Struct for second part of single species getting info in second request
type SpeciesY struct {
	SpiecesY string `json:"year"`
}

//Struct for info about diag
type Diag struct {
	Gbif          int     `json:"gbifstatus"`
	RestCountries int     `json:"restcountries"`
	Version       string  `json:"version"`
	Uptime        float64 `json:"uptime"`
}

//Checks if an array of integers contains one specific integer
func checkSpeciesKey(speciesKeyArray []int, speciesKey int) bool {
	for _, sp := range speciesKeyArray {
		if sp == speciesKey {
			return false
		}
	}
	return true
}

// Get Country
func getCountry(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r) // Get Params
	code := params["country_identifier"]
	limit := r.FormValue("limit")
	response, err := http.Get("https://restcountries.eu/rest/v2/alpha/" + code)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		http.Error(w, "Error", http.StatusInternalServerError)
		return
	}
	response2, err := http.Get("https://api.gbif.org/v1/occurrence/search?country=" + code + "&limit=" + limit)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return
	}

	if response2.StatusCode != 200 {
		fmt.Printf("Invalid output, StatusCode: %d", response2.StatusCode)
	}

	//Variables for structs
	var country Country
	var results Results
	var output speciesOutput

	err = json.NewDecoder(response.Body).Decode(&country)
	if err != nil {
		http.Error(w, "Error", http.StatusInternalServerError)
		return
	}

	err = json.NewDecoder(response2.Body).Decode(&results)
	if err != nil {
		http.Error(w, "Error", http.StatusInternalServerError)
		return
	}

	output.CountryCode = country.CountryCode
	output.CountryFlag = country.CountryFlag
	output.CountryName = country.CountryName

	for _, species := range results.Result {
		if checkSpeciesKey(output.SpeciesKey, species.SpeciesKey) {
			output.Species = append(output.Species, species.Species)
			output.SpeciesKey = append(output.SpeciesKey, species.SpeciesKey)
		}
	}
	json.NewEncoder(w).Encode(output)
}

//function for getting a single species
func getSpecies(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r) // Get Params
	specieskey := params["speciesKey"]

	response, err := http.Get("https://api.gbif.org/v1/species/" + specieskey)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		http.Error(w, "Error", http.StatusInternalServerError)
		return
	}

	var species SingleSpecies

	err = json.NewDecoder(response.Body).Decode(&species)
	if err != nil {
		http.Error(w, "Error", http.StatusInternalServerError)
		return
	}

	response2, err := http.Get("https://api.gbif.org/v1/species/" + specieskey + "/name")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		http.Error(w, "Error", http.StatusInternalServerError)
		return
	}

	var speciesYear SpeciesY

	err = json.NewDecoder(response2.Body).Decode(&speciesYear)
	if err != nil {
		fmt.Printf("Error her %s", err)
		http.Error(w, "Error", http.StatusInternalServerError)
		return
	}

	species.SpeciesYear = speciesY.SpiecesY

	json.NewEncoder(w).Encode(species)
}

//Function for getting Diag
func getDiag(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var diag Diag
	response, err := http.Get("https://api.gbif.org/v1/")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		http.Error(w, "Error", http.StatusInternalServerError)
		return
	}
	if response.StatusCode != 200 {
		fmt.Printf("Invalid output, StatusCode: %d", response.StatusCode)
		return
	}
	diag.Gbif = response.StatusCode
	err = json.NewDecoder(response.Body).Decode(&diag)

	response2, err := http.Get("https://restcountries.eu/rest/v2/")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		http.Error(w, "Error", http.StatusInternalServerError)
		return
	}
	if response2.StatusCode != 200 {
		fmt.Printf("Invalid output, StatusCode: %d", response.StatusCode)
		return
	}
	diag.RestCountries = response2.StatusCode
	err = json.NewDecoder(response2.Body).Decode(&diag)

	uptime := time.Since(startTime)

	var secondsUptime = uptime.Seconds()

	diag.Version = "v1"
	diag.Uptime = secondsUptime
	json.NewEncoder(w).Encode(diag)
}

func main() {
	// Init Router

	port := os.Getenv("PORT")

	r := mux.NewRouter()

	//Route Handlers / Endpoints
	r.HandleFunc("/conservation/v1/country/{country_identifier}", getCountry).Methods("GET")
	r.HandleFunc("/conservation/v1/species/{speciesKey}", getSpecies).Methods("GET")
	r.HandleFunc("/conservation/v1/diag", getDiag).Methods("GET")

	log.Fatal(http.ListenAndServe(":"+port, r))
}
